from rest_framework import serializers
from mm.models import Product
from mm.serializers.CategorySeializer import CategorySerializers
from MobiMarket import settings


class AddProductSerializers(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'product_category',
            'name',
            'barcode',
            'price',
            'quantity',
            'sale',
            'total_price',
            'total_sale_price'
        ]


class UpdateProductSerializers(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'id',
            'number',
            'product_category',
            'name',
            'barcode',
            'price',
            'quantity',
            'sale',
        ]


class ProductSerializers(serializers.ModelSerializer):
    product_category = CategorySerializers()
    total_price = serializers.DecimalField(max_digits=5, decimal_places=2)
    total_sale_price = serializers.DecimalField(max_digits=5, decimal_places=2)
    created = serializers.DateTimeField(format=settings.DATETIME_FORMAT, input_formats=None)

    class Meta:
        model = Product
        fields = [
            'id',
            'number',
            'product_category',
            'created',
            'name',
            'barcode',
            'price',
            'quantity',
            'sale',
            'total_price',
            'total_sale_price'
        ]
