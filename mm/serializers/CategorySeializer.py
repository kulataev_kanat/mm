from rest_framework import serializers

from mm.models import Category


class CategorySerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
