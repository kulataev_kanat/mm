from django.contrib import admin

from mm.models import Category, Product, User

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(User)
