import cloudmersive_barcode_api_client
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework import viewsets

from mm import own_viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from MobiMarket.barcode_generate import random_with_N_digits
from mm.authentication import SafeJWTAuthentication
from mm.models import Product
from mm.serializers.ProductSerializers import AddProductSerializers, UpdateProductSerializers, ProductSerializers


class FindProductByBarcode(viewsets.ReadOnlyModelViewSet):
    configuration = cloudmersive_barcode_api_client.Configuration()
    configuration.api_key['Apikey'] = 'ef8cded7-aaef-4cbd-bed0-88613bad4b95'
    api_instance = cloudmersive_barcode_api_client.BarcodeLookupApi(
        cloudmersive_barcode_api_client.ApiClient(configuration))


class ProductFindByBarcode(viewsets.ReadOnlyModelViewSet):
    filter_backends = [filters.SearchFilter]
    search_fields = ['^barcode']
    queryset = Product.objects.all()
    serializer_class = ProductSerializers

    @action(detail=False)
    def bargen(self, request, pk=None):
        barcode = random_with_N_digits(13)
        return Response(barcode)

    def get_permissions(self):
        permission_class = []
        if self.action == 'list':
            permission_class = [AllowAny]
        elif self.action == 'retrieve' or self.action == 'update':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]


class AddProductViewSet(own_viewsets.CreateModelViewSet):
    queryset = Product.objects.all()
    serializer_class = AddProductSerializers

    authentication_classes = [SafeJWTAuthentication]

    def get_permissions(self):
        permission_class = []
        if self.action == 'create':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]


class RemoveProductViewSet(own_viewsets.DestroyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializers

    def get_permissions(self):
        permission_class = []
        if self.action == 'destroy':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]


class UpdateProductViewSet(own_viewsets.UpdateModelViewSet):
    queryset = Product.objects.all()
    serializer_class = UpdateProductSerializers

    def get_permissions(self):
        permission_class = []
        if self.action == 'retrieve' or self.action == 'update':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]


class GetProductViewSet(own_viewsets.GetModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializers

    authentication_classes = [SafeJWTAuthentication]

    def get_permissions(self):
        permission_class = []
        if self.action == 'create':
            permission_class = [AllowAny]
        elif self.action == 'list':
            permission_class = [AllowAny]
        elif self.action == 'retrieve' or self.action == 'update':
            permission_class = [AllowAny]
        elif self.action == 'destroy':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]
