import jwt
from django.contrib.auth import get_user_model
from rest_framework import exceptions
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from MobiMarket import settings
from mm.provider import generate_access_token, generate_refresh_token
from mm.serializers import UserSerializer


@api_view(['POST'])
@permission_classes([AllowAny])
def access_token_view(request):
    User = get_user_model()
    username = request.data.get('username')
    password = request.data.get('password')
    response = Response()

    if (username is None) or (password is None):
        raise exceptions.AuthenticationFailed('username and password required')

    user = User.objects.filter(username=username).first()

    if user is None:
        raise exceptions.AuthenticationFailed('user not found')

    if not user.check_password(password):
        raise exceptions.AuthenticationFailed('wrong password')

    serialized_user = UserSerializer(user).data

    access_token = generate_access_token(user)
    refresh_token = generate_refresh_token(user)

    response.set_cookie(key='refresh_token', value=refresh_token, httponly=True)
    response.data = {
        'access_token': access_token,
        'user': serialized_user,
    }

    return response


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token_view(request):
    User = get_user_model()
    refresh_token = request.COOKIES.get('refresh_token')

    if refresh_token is None:
        raise exceptions.AuthenticationFailed('Authentication credentials were not provided.')
    try:
        payload = jwt.decode(refresh_token, settings.REFRESH_TOKEN_SECRET, algorithms=['HS256'])

    except jwt.ExpiredSignatureError:
        raise exceptions.AuthenticationFailed('expired refresh token, please login again.')

    user = User.objects.filter(id=payload.get('user_id')).first()

    if user is None:
        raise exceptions.AuthenticationFailed('User not found')

    if not user.is_active:
        raise exceptions.AuthenticationFailed('user is inactive')

    access_token = generate_access_token(user)

    return Response({'access_token': access_token})
