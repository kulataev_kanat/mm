from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from mm.authentication import SafeJWTAuthentication
from mm.models import Category
from mm.serializers.CategorySeializer import CategorySerializers


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers

    authentication_classes = [SafeJWTAuthentication]

    def get_permissions(self):
        permission_class = []
        if self.action == 'create':
            permission_class = [AllowAny]
        elif self.action == 'list':
            permission_class = [AllowAny]
        elif self.action == 'retrieve' or self.action == 'update':
            permission_class = [AllowAny]
        elif self.action == 'destroy':
            permission_class = [AllowAny]
        return [permission() for permission in permission_class]
