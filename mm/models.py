import operator
from decimal import Decimal

from django.contrib.auth.models import AbstractUser, Group
from django.db import models
from MobiMarket.models import BaseModel


class User(AbstractUser):
    groups = models.ForeignKey(Group, on_delete=models.CASCADE, default=1)
    email = models.EmailField(max_length=50, unique=True)

    REQUIRED_FIELDS = ['groups_id', 'email']

    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name


class Category(BaseModel):
    name = models.CharField(max_length=30, blank=True, default='category_name')


class Product(BaseModel):
    number = models.IntegerField(auto_created=True, blank=True, default=0)
    product_category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now=True)
    # created = models.DateTimeField(auto_now_add=True)
    # updated = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=30, blank=True, default='product_name')
    price = models.DecimalField(max_digits=5, decimal_places=2)
    barcode = models.BigIntegerField(blank=False, default='2998060398046')
    quantity = models.IntegerField(blank=False, default='1')
    sale = models.IntegerField(blank=False, default='0')

    def save(self, **kwargs):
        self.number = Product.objects.order_by('number').last().number + 1
        super(Product, self).save()

    @property
    def total_price(self):
        price = Decimal(self.price.__str__())
        quantity = Decimal(self.quantity.__str__())
        return operator.__mul__(price, quantity)

    @property
    def total_sale_price(self):
        total_price = Decimal(self.total_price.__str__())
        sale = Decimal(self.sale.__str__())
        quantity = Decimal(self.quantity.__str__())
        return operator.__sub__(total_price, operator.__mul__(sale, quantity))
