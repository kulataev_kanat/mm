from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from mm.views.CategoryView import CategoryViewSet
from mm.views.ProductView import AddProductViewSet, RemoveProductViewSet, UpdateProductViewSet, GetProductViewSet, \
    ProductFindByBarcode
from mm.views.UserView import access_token_view, refresh_token_view

router = DefaultRouter()
router.register('category', CategoryViewSet, basename='category'),
router.register('add_product', AddProductViewSet, basename='add_product'),
router.register('remove_product', RemoveProductViewSet, basename='remove_product'),
router.register('update_product', UpdateProductViewSet, basename='update_product'),
router.register('get_product', GetProductViewSet, basename='get_product'),
router.register('findByBarcode', ProductFindByBarcode, basename='findByBarcode'),

urlpatterns = [
    url('', include(router.urls)),
    url('access/', access_token_view, name='access'),
    url('refresh/', refresh_token_view, name='refresh'),
]
